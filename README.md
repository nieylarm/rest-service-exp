# Spring Boot + Hibernate + PostgreSQL

Experimenting with Spring Boot and Hibernate over PostgreSQL.

## Run it
```gradle bootRun```
> _Note:_ Changes on Java source files will automatically restart the Spring server.

## Testing it
There is a postman script that can be used to rudimentarily test the application when it is running. The file is [here](./docs/rest-service-exp.postman_collection.json). Once downloaded, it can be imported into Postman.