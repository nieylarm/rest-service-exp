package ext;

import app.model.Content;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class ExtController {

    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/ext")
    public Content ext(@RequestParam(value="value", defaultValue="") String value) {
        return new Content(counter.incrementAndGet(), value);
    }
}
