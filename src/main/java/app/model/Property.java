package app.model;

import javax.persistence.*;

@Entity
@Table(name = "property", uniqueConstraints = @UniqueConstraint(columnNames = { "name", "contentId" }))
public class Property implements IProperty {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "property", fetch = FetchType.EAGER)
    private PropertyString propertyString;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "property", fetch = FetchType.EAGER)
    private PropertyLong propertyLong;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "property", fetch = FetchType.EAGER)
    private PropertyDouble propertyDouble;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "property", fetch = FetchType.EAGER)
    private PropertyDate propertyDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contentId")
    private Content content;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PropertyString getPropertyString() {
        return propertyString;
    }

    public void setPropertyString(PropertyString propertyString) {
        this.propertyString = propertyString;
    }

    public PropertyLong getPropertyLong() {
        return propertyLong;
    }

    public void setPropertyLong(PropertyLong propertyLong) {
        this.propertyLong = propertyLong;
    }

    public PropertyDouble getPropertyDouble() {
        return propertyDouble;
    }

    public void setPropertyDouble(PropertyDouble propertyDouble) {
        this.propertyDouble = propertyDouble;
    }

    public PropertyDate getPropertyDate() {
        return propertyDate;
    }

    public void setPropertyDate(PropertyDate propertyDate) {
        this.propertyDate = propertyDate;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

}
