package app.model;

public interface IProperty {
    public long getId();

    public String getName();

    public Content getContent();
}
