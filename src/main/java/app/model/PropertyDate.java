package app.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "propertyDate")
public class PropertyDate {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;
    private Date value;

    @OneToOne
    @JoinColumn(name = "propertyId")
    private Property property;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getValue() {
        return value;
    }

    public void setValue(Date value) {
        this.value = value;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

}
