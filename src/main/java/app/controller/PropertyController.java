package app.controller;

import app.dao.PropertyDao;
import app.model.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PropertyController {

    @Autowired
    private PropertyDao propertyDao;

    @RequestMapping("/properties")
    public ResponseEntity<Iterable<Property>> contents() {
        Iterable<Property> properties = propertyDao.findAll();
        return new ResponseEntity<Iterable<Property>>(properties, HttpStatus.OK);
    }

    @DeleteMapping("/property/deleteAll")
    public String deleteAll() {
        propertyDao.deleteAll();
        return HttpStatus.OK.toString();
    }
}
