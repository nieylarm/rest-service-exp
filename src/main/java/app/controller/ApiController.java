package app.controller;

import app.model.Content;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class ApiController {

    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/test")
    public Content test(@RequestParam(value="value", defaultValue="no value found!!!") String value) {
        return new Content(counter.incrementAndGet(), value);
    }
}
