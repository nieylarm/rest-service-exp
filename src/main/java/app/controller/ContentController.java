package app.controller;

import app.dao.ContentDao;
import app.dao.PropertyDao;
import app.model.Content;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class ContentController {

    private final AtomicLong counter = new AtomicLong();

    @Autowired
    private ContentDao contentDao;
    @Autowired
    private PropertyDao propertyDao;

    @PostMapping("/content/create")
    public ResponseEntity<Content> create(@RequestBody Content content) {
        contentDao.save(content);
        return new ResponseEntity<Content>(content, HttpStatus.CREATED);
    }

    @PutMapping("/content/update")
    public ResponseEntity<Content> update(@RequestBody Content content) {
        contentDao.save(content);
        return new ResponseEntity<Content>(content, HttpStatus.OK);
    }

    @DeleteMapping("/content/delete/{id}")
    public ResponseEntity<Content> delete(@PathVariable("id") long id) {
        Content content = contentDao.findOne(id);
        contentDao.delete(content);
        return new ResponseEntity<Content>(content, HttpStatus.OK);
    }

    @DeleteMapping("/content/deleteAll")
    public String deleteAll() {
        contentDao.deleteAll();
        return HttpStatus.OK.toString();
    }

    @RequestMapping("/contents")
    public ResponseEntity<Iterable<Content>> contents() {
        Iterable<Content> contents = contentDao.findAll();
        return new ResponseEntity<Iterable<Content>>(contents, HttpStatus.OK);
    }

    @RequestMapping("/content/test")
    public Content test(@RequestParam(value="value", defaultValue="no value found!!!") String value) {
        return new Content(counter.incrementAndGet(), value);
    }
}
