package app.dao;

import app.model.Property;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface PropertyDao extends CrudRepository<Property, Long> {

}
