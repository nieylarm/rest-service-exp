package app.dao;

import app.model.Content;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface ContentDao extends CrudRepository<Content, Long> {

}
